﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Zeigt die Spieler-Gesundheit in Form eines Fortschrittsbalkens.
/// </summary>
public class HealthBar : MonoBehaviour
{
    /// <summary>
    /// Zeiger auf die Grafik, die skaliert wird.
    /// </summary>
    public Image progressbar;

    /// <summary>
    /// Zeiger auf die aktuelle Spieler-Komponente.
    /// </summary>
    private Player player;

    // Update is called once per frame
    private void Update()
    {
        if (player == null)
            player = FindObjectOfType<Player>();
        else
            progressbar.fillAmount = player.health;
    }
}
