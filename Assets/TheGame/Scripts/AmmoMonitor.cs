﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Zeigt die Anzahl der Patronen in der Pistole.
/// </summary>
public class AmmoMonitor : MonoBehaviour
{

    /// <summary>
    /// Zeiger auf das UI-Text-Objekt.
    /// </summary>
    public Text uiText;

    /// <summary>
    /// Zeiger auf die aktuelle Waffe des Spielers.
    /// </summary>
    private Gun gun;

    // Update is called once per frame
    private void Update()
    {
        if (gun == null)
        {
            Player p = FindObjectOfType<Player>();
            gun = p.GetComponentInChildren<Gun>();
        }
        else
            uiText.text = gun.ammo.ToString();
    }
}
