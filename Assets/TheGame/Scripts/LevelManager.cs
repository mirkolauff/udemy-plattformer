﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{

    private void Awake()
    {
        SaveGameData.current = SaveGameData.load();
    }

    private void Start()
    {
        loadScene(SaveGameData.current.recentScene);
    }

    public void loadScene(string name)
    {
        if (name == "")
            return; //ungültiger aufruf

        for (int i = SceneManager.sceneCount - 1; i > 0; i = i - 1)
        {
            SceneManager.UnloadSceneAsync(SceneManager.GetSceneAt(i).name);
        }


        Debug.Log("Lade jetzt szene:" + name);
        SceneManager.LoadScene(name, LoadSceneMode.Additive);
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Alpha1))
            SaveGameData.current = SaveGameData.load();
        else if (Input.GetKeyUp(KeyCode.Alpha2))
            SaveGameData.current.save();
    }
}
